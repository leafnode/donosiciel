package pl.waw.gdzieonikurdesa.donosiciel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQL_Helper extends SQLiteOpenHelper {
	public static final String TABLE_GPS = "gps";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TIMESTAMP = "f_timestamp";
	public static final String COLUMN_LATITUDE = "f_latitude";
	public static final String COLUMN_LONGITUDE = "f_longitude";
	public static final String COLUMN_ALTITUDE = "f_altitude";
	public static final String COLUMN_ACCURACY = "f_accuracy";
	public static final String COLUMN_BEARING = "f_bearing";
	public static final String COLUMN_SPEED = "f_speed";
	public static final String COLUMN_PROVIDER = "f_provider";

	private static final String DATABASE_NAME = "donosiciel.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_GPS + "(" + COLUMN_ID
			+ " INTEGER primary key autoincrement, " + COLUMN_TIMESTAMP
			+ " TIMESTAMP not null, " + COLUMN_LATITUDE
			+ " DOUBLE PRECISION not null, " + COLUMN_LONGITUDE
			+ " DOUBLE PRECISION not null, " + COLUMN_ALTITUDE
			+ " DOUBLE PRECISION not null, " + COLUMN_SPEED
			+ " FLOAT not null, " + COLUMN_ACCURACY + " FLOAT not null, "
			+ COLUMN_BEARING + " FLOAT, " + COLUMN_PROVIDER + " VARCHAR(256));";

	public SQL_Helper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQL_Helper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GPS);
		onCreate(db);
	}
}
