package pl.waw.gdzieonikurdesa.donosiciel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;

public class SyncAdapter {
	SQL_DataSource db = null;

	// private String HOST = "http://dlakasi.pl/";
	private String HOST = "http://gdzieonikurdesa.pl/";
	// private String HOST = "http://192.168.1.6/spyzlom/";
	private String USER_NAME = null;
	private String PASSWORD = null;

	public SyncAdapter(SQL_DataSource db, String user, String pass) {
		this.db = db;
		this.USER_NAME = user;
		this.PASSWORD = pass;
	}

	public void sync() throws Exception {
		String oldTimeStamp = getLastTimeStamp();
		List<HashMap<String, String>> samples = db
				.getNotSyncedSamples(oldTimeStamp);

		if (samples.size() == 1)
			addNewSample(samples.get(0));
		else {
			JSONArray mJSONArray = new JSONArray();
			for (int i = 0; i < samples.size(); i++) {
				mJSONArray.put(new JSONObject(samples.get(i)));
			}
			Log.i("INFO", "Sending big JSON with samples");
			pushSamples(mJSONArray);
		}
	}

	public String getLastTimeStamp() throws Exception {
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used. 
		int timeoutConnection = 10000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient mHttpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(HOST + "default/getLastValidSample");
		String credentials = USER_NAME + ":" + PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(
				credentials.getBytes(), Base64.NO_WRAP);
		httpget.addHeader("Authorization", "Basic " + base64EncodedCredentials);

		HttpResponse response = mHttpClient.execute(httpget);
		if (response.getStatusLine().getStatusCode() != 200)
			throw new HttpResponseException(response.getStatusLine()
					.getStatusCode(), "Fuckup z uwierzytelnianiem: " + USER_NAME + " " + PASSWORD );

		String data = EntityUtils.toString(response.getEntity());
		mHttpClient.getConnectionManager().shutdown();
		return data;
	}

	public void addNewSample(HashMap<String, String> s) throws Exception {
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used. 
		int timeoutConnection = 10000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient mHttpClient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(HOST + "default/api/samples");
		String credentials = USER_NAME + ":" + PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(
				credentials.getBytes(), Base64.NO_WRAP);
		httppost.addHeader("Authorization", "Basic " + base64EncodedCredentials);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("f_timestamp", s
				.get("f_timestamp")));
		nameValuePairs.add(new BasicNameValuePair("f_latitude", s
				.get("f_latitude")));
		nameValuePairs.add(new BasicNameValuePair("f_longitude", s
				.get("f_longitude")));
		nameValuePairs.add(new BasicNameValuePair("f_altitude", s
				.get("f_altitude")));
		nameValuePairs.add(new BasicNameValuePair("f_speed", s.get("f_speed")));
		nameValuePairs.add(new BasicNameValuePair("f_accuracy", s
				.get("f_accuracy")));
		nameValuePairs.add(new BasicNameValuePair("f_bearing", s
				.get("f_bearing")));
		nameValuePairs.add(new BasicNameValuePair("f_provider", s
				.get("f_provider")));

		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		@SuppressWarnings("unused")
		HttpResponse response = mHttpClient.execute(httppost);

		mHttpClient.getConnectionManager().shutdown();
	}

	public void pushSamples(JSONArray json) throws Exception {
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used. 
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 30000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient mHttpClient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(HOST + "default/pushSamples");
		String credentials = USER_NAME + ":" + PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(
				credentials.getBytes(), Base64.NO_WRAP);
		httppost.addHeader("Authorization", "Basic " + base64EncodedCredentials);
		httppost.setHeader("Content-type", "application/json");

		StringEntity se = new StringEntity(json.toString());
		httppost.setEntity(se);

		HttpResponse response = mHttpClient.execute(httppost);
		Log.i("RESPONSE", response.getEntity().toString());
		mHttpClient.getConnectionManager().shutdown();
	}
}
