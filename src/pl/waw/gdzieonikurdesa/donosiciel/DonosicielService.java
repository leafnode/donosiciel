package pl.waw.gdzieonikurdesa.donosiciel;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import pl.waw.gdzieonikurdesa.donosiciel.R;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class DonosicielService extends Service {
	private final IBinder mBinder = new LocalBinder();
	private SQL_DataSource mDb;
	private Boolean isUpdating = false;

	private String USER_NAME = "";
	private String PASSWORD = "";

	public class LocalBinder extends Binder {
		DonosicielService getService() {
			return DonosicielService.this;
		}
	}

	public static final long GPS_INTERVAL = 60000;
	public static final long SMALLEST_DIST = 50;

	private NotificationManager mNm;
	private ConnectivityManager mCm;
	private ScheduledThreadPoolExecutor mExecutor;
	private PowerDetector mPD;

	// GOOGLE PLAY SERVICES
	private LocationClient mLC;

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		SharedPreferences settings = getSharedPreferences(Donosiciel.PREFS_NAME, 0);
		USER_NAME = settings.getString("email", "ciul");
		PASSWORD = settings.getString("passw", "has�o");
		
		mNm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mCm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		mExecutor = new ScheduledThreadPoolExecutor(2);
		mDb = new SQL_DataSource(getApplicationContext());
		mDb.open();

		mLC = new LocationClient(this,
				new ConnectionCallbacks() {

			@Override
			public void onDisconnected() {
				showNotification(getResources().getString(R.string.app_name), getResources().getString(R.string.dservice_stop), null, false);
			}

			@Override
			public void onConnected(Bundle connectionHint) {
				showNotification(getResources().getString(R.string.app_name), getResources().getString(R.string.dservice_start), null, false);

				LocationRequest lr = LocationRequest.create();
				lr.setPriority(100);
				lr.setInterval(GPS_INTERVAL);
				lr.setSmallestDisplacement(SMALLEST_DIST);

				mLC.requestLocationUpdates(lr, mLocationListener);
			}

		}, new OnConnectionFailedListener() {

			public void onConnectionFailed(ConnectionResult result) {
				showNotification(getResources().getString(R.string.app_name),
						getResources().getText(R.string.dservice_conFailed), null, false);
			}
		});		

		mPD = new PowerDetector();
		this.registerReceiver(mPD, new IntentFilter(
				Intent.ACTION_POWER_CONNECTED));
		this.registerReceiver(mPD, new IntentFilter(
				Intent.ACTION_POWER_DISCONNECTED));
		
		mLC.connect();
	}

	private LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			mExecutor.schedule(new SampleSync(location), 0, TimeUnit.SECONDS);
		}
	};

	private class SampleSync implements Runnable {
		private Location l;

		public SampleSync(Location l) {
			this.l = l;
		}

		@Override
		public void run() {
			try {
				NetworkInfo mNetworkInfo = mCm.getActiveNetworkInfo();
				Boolean updatePossible = false;
				if (mNetworkInfo != null)
					updatePossible = mNetworkInfo.isAvailable()
							&& mNetworkInfo.isConnected();

				mDb.open();
				mDb.createSample(this.l.getTime(), this.l.getLatitude(),
						this.l.getLongitude(), this.l.getAltitude(),
						(float) (this.l.getSpeed() * 3.6),
						this.l.getAccuracy(), this.l.getBearing(),
						this.l.getProvider());
				
				java.util.Date timeStamp = new java.util.Date(this.l.getTime());
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss,SSS", new Locale("pl"));
				showNotification(getResources().getString(R.string.app_name),
						"Timestamp: " + sdf.format(timeStamp), null,
						updatePossible);
				
				if (!isUpdating && updatePossible) {
					SyncAdapter mSyncAdapter = new SyncAdapter(mDb, USER_NAME,
							PASSWORD);
					try {
						isUpdating = true;
						showNotification(
								getResources().getString(R.string.app_name),
								getResources().getText(R.string.sending), null,
								updatePossible);
						mSyncAdapter.sync();
						showNotification(
								getResources().getString(R.string.app_name),
								getResources()
										.getText(R.string.sending_success),
								null, updatePossible);
					} catch (Exception e) {
						Log.w("SYNCADAPTER", e.toString());
						showNotification(
								getResources().getString(R.string.app_name),
								getResources().getText(R.string.sending_failed),
								getResources().getText(R.string.sending_failed)
										+ "\n" + e.toString(), updatePossible);
					} finally {
						isUpdating = false;
					}
				}				
			} catch (Exception ex) {
				Toast.makeText(getApplicationContext(), ex.toString(),
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			String action = intent.getAction();
			Bundle bundle = intent.getExtras();
			if (action != null) {
				if (action.equals(Intent.ACTION_POWER_CONNECTED)) {

					// Get Location Manager and check for GPS & Network location
					// services
					LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
					if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
							|| !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
						try {
							Intent i = new Intent(
									Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(i);
						} catch (Exception e) {
							Toast.makeText(getApplicationContext(),
									e.toString(), Toast.LENGTH_SHORT).show();
						}
					}
				}
				if (action.equals("CONFIG") && bundle != null) {
					USER_NAME = (String) intent.getExtras().get("USER_NAME");
					PASSWORD = (String) intent.getExtras().get("PASSWORD");
				}
			}
		} catch (Exception ex) {
			Log.e("ERROR ON STARTCMD", ex.toString());
			SharedPreferences settings = getSharedPreferences(Donosiciel.PREFS_NAME, 0);
			USER_NAME = settings.getString("email", "ciul");
			PASSWORD = settings.getString("passw", "has�o");
		}
		return START_STICKY;
	}

	@Override
	public void onDestroy() {		
		mLC.disconnect();
		mExecutor.shutdown();
		unregisterReceiver(mPD);
		mNm.cancel(R.string.app_name);
		mDb.close();
		super.onDestroy();
	}

	private void showNotification(CharSequence title, CharSequence text,
			CharSequence bigText, Boolean online) {
		mNm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		int notifyID = R.string.app_name;

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, Donosiciel.class), 0);
		NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
				this).setContentTitle(title).setContentText(text)
				.setOngoing(true).setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(contentIntent)
				.setLights(0xff0000ff, 1000, 1000);
		
		if (online) mNotifyBuilder.setSubText(getResources().getText(R.string.online));
		else mNotifyBuilder.setSubText(getResources().getText(R.string.offline));

		if (bigText != null) {
			mNotifyBuilder.setStyle(new NotificationCompat.BigTextStyle()
					.bigText(bigText));
		}

		mNm.notify(notifyID, mNotifyBuilder.build());
	}
}
