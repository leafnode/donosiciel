package pl.waw.gdzieonikurdesa.donosiciel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PowerDetector extends BroadcastReceiver {
 
	@Override
	public void onReceive(Context context, Intent intent) {
 
		String action = intent.getAction();
 		if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
 	    	Intent i = new Intent(context, DonosicielService.class);
 	    	i.setAction(Intent.ACTION_POWER_CONNECTED);
 	    	context.startService(i);
 	    } if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
	    	Intent i = new Intent(context, DonosicielService.class);
	    	i.setAction(Intent.ACTION_POWER_DISCONNECTED);
	    	context.startService(i);
	    }
 
	}
 
}