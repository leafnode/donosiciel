package pl.waw.gdzieonikurdesa.donosiciel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import pl.waw.gdzieonikurdesa.donosiciel.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class DescFailTask extends AsyncTask<String, Void, JSONObject>{
	private static final int CONN_TIMEOUT = 10000;
	private static final int SOCKET_TIMEOUT = 5000;
	
	private String HOST = "http://gdzieonikurdesa.pl/";

	private String USER_NAME = null;
	private String PASSWORD = null;

	private long synced = -1l;
	private String desc = null;
	private Boolean fail = false;
	private Donosiciel ctx = null;
	
	public DescFailTask(Donosiciel app, String user, String pass){
		this.ctx = app;
		this.USER_NAME = user;
		this.PASSWORD = pass;
	}
	
	@Override
    protected JSONObject doInBackground(String... cmd) {
		if (cmd.length < 1) return null;
    	//Log.d("X", "DFT: " + cmd[0]);

		try {
			if ("GET".equals(cmd[0]))
				return getDescFail();
			else if ("SET".equals(cmd[0]))
				return setDescFail(cmd[1], cmd[2]);
		} catch (IOException e) {
			return msg("ioexp", "1");
		} catch (JSONException e) {
			return msg("jsonexp", "1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected void onPostExecute(JSONObject result) {
		Toast toast = null;
		if (result == null)
			toast = Toast.makeText(this.ctx, R.string.df_fail, Toast.LENGTH_LONG);
		else if (result.has("ioexp"))
			toast = Toast.makeText(this.ctx, R.string.df_netfail, Toast.LENGTH_LONG);
		else if (result.has("jsonexp"))
			toast = Toast.makeText(this.ctx, R.string.df_jsonexp, Toast.LENGTH_LONG);
		else if (result.has("yyyyy"))
			toast = Toast.makeText(this.ctx, R.string.df_yyyyy, Toast.LENGTH_LONG);
		else if (result.has("forbinden"))
			toast = Toast.makeText(this.ctx, R.string.df_forbinden, Toast.LENGTH_LONG);
		else if (result.has("updatederrors"))
			toast = Toast.makeText(this.ctx, R.string.df_saved, Toast.LENGTH_LONG);
		else if (result.has("f_failure")){
			String desc = null;
			Boolean fail = null;
			try {
				desc = result.getString("f_failure_desc");
				fail = result.getBoolean("f_failure");
				toast = Toast.makeText(this.ctx, R.string.df_get, Toast.LENGTH_LONG);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			ctx.setDescFail(desc == null || desc.length() < 1 ? "" : desc, 
					fail == null ? false : fail);
		}
		else if (result.has("error")){
			try {
				String body = result.getString("error");
				if (body.length() > 1 && body.length() < 50)
					toast = Toast.makeText(this.ctx, "Co�ik dziwego przyjsz�o: " + body, Toast.LENGTH_LONG);
				else
					toast = Toast.makeText(this.ctx, R.string.df_yyyyy, Toast.LENGTH_LONG);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		this.ctx.setSync();
		
		if (toast != null)
			toast.show();
    }

	private JSONObject setDescFail(String desc, String fail) throws Exception {
		JSONObject obj;
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, CONN_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, SOCKET_TIMEOUT);
		
		HttpClient mHttpClient = new DefaultHttpClient();
		HttpPut req = new HttpPut(HOST + "default/uapi.json");
		String credentials = USER_NAME + ":" + PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
		req.addHeader("Authorization", "Basic " + base64EncodedCredentials);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("f_failure", fail));
		nameValuePairs.add(new BasicNameValuePair("f_failure_desc", desc));
		req.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = mHttpClient.execute(req);
		StatusLine sl = response.getStatusLine();
		//Log.d("X", sl.toString());
        
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);
        out.close();
        
        if (sl.getStatusCode() == HttpStatus.SC_OK){
	        String body = out.toString();
	        if (body.length() < 15)
	        	obj = msg(body, "1");
	        else
	        	obj = msg("error", body);
        }
        else if (sl.getStatusCode() == HttpStatus.SC_FORBIDDEN)
        	obj = msg("forbinden", "1");
        else
        	obj = msg("yyyyy", "1");

		mHttpClient.getConnectionManager().shutdown();
		
		return obj;
	}

	private JSONObject getDescFail() throws Exception {
		JSONObject obj;
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, CONN_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, SOCKET_TIMEOUT);
		
		HttpClient mHttpClient = new DefaultHttpClient();
		HttpGet req = new HttpGet(HOST + "default/uapi/me/failure.json");
		String credentials = USER_NAME + ":" + PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
		req.addHeader("Authorization", "Basic " + base64EncodedCredentials);

		HttpResponse response = mHttpClient.execute(req);
		StatusLine sl = response.getStatusLine();
		//Log.d("X", sl.toString());
        
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);
        out.close();
        
        // Proces object
        if (sl.getStatusCode() == HttpStatus.SC_OK){
	        obj = new JSONObject(out.toString());
	    	obj = obj.getJSONArray("content").getJSONObject(0);
	        
	        Log.d("X", obj.toString());
        }
        else if (sl.getStatusCode() == HttpStatus.SC_FORBIDDEN)
        	obj = msg("forbinden", "1");
        else
        	obj = msg("yyyyy", "1");

		mHttpClient.getConnectionManager().shutdown();

		return obj;
	}

	private JSONObject msg(String key, String msg){
		JSONObject obj = new JSONObject();
		try {
			obj.put(key, msg);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}
}
